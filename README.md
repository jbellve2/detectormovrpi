### Detector de moviment Raspberry pi, que enviarà un missatge Telegram

Projecte amb Raspberry pi i en python per capturar la senyal que emet un mòdul de moviment PIR. 
En el moment que detecta moviment, entrarà un senyal i enviarà un missatge Telegram, utilitzant un l'script pythonTelegramIOC.py

![diagrama.png](Images/diagrama.png)


## Servei Telegram

Caldrà tenir un Token i un Chat_id de Telegram. Es pot fer amb el servei botFather de Telegram.

Per utilitzar el servei de Telegram des de python, hem d’aconseguir un ```token``` i un ```chat_id```. Aquestes dades han de ser privades ja que qualsevol que les tinga podrà rebre i enviar missatges.

+ **Token:** Número d’identificador únic del nostre bot Telegram.
+ **Chat_id:** Identificador del xat del grup on volem enviar missatges.

# 1.1 BotFather

Telegram té un bot anomenat [@BotFather](https://telegram.me/BotFather), que ens permet crear i obtenir el Token i així treballar amb l'API de Bots de Telegram. 

Per obtenir aquest bot, hem d’obrir el nostre Telegram i cercar el canal [@BotFather](https://telegram.me/BotFather) 

![botFather.png](Images/botFather.png)

Ordres bàsiques
+ **/newbot** -> Crea un bot nou.
+ **/token** -> Genera un token d'autorització.
+ **/revoke** -> Permet revocar el token d'accés a un bot, generant un nou token.
+ **/setname** -> Canvia el nom del bot que triem a la llista.
+ **/setdescription** -> Canvia la descripció del bot.
+ **/deletebot** -> Elimina el bot
+ **/cancel** -> Cancel·la l'operació actual


# 1.2 Token
Per obtenir-lo hem d’anar al nostre bot **[@BotFather](https://telegram.me/BotFather)**. Es pot accedir tant pel Telegram web com per l’app Telegram del mòbil, i en primer lloc introduirem la comanda ```/newbot```. El programa ens demanarà un nom de bot i un usuari del bot (pot ser el mateix que el nom del bot, però l’usuari ha de ser únic).
Un cop acabem aquesta part ens generarà el token que utilitzarem per identificar el nostre bot des de l’script de python


![token.png](Images/token.png)

**IMPORTANT** → El token dóna accés complet al nostre bot i per raons de seguretat no s’ha de revelar el token a tercers, en cas que exposéssim el token del nostre bot sense adonar-nos podem revocar el token actual i generar-ne un de nou usant l’ordre revoke a BotFather.

En el nostre taller podeu utilitzar el token que se us proporcionarà a través de la pràctica o generar un token propi que podreu eliminar després.

Amb l’ordre ```mybots``` llistareu els bots que teniu i podreu veure aquest que acabem de crear ara.


# 1.3 chat_id
Ja tenim el bot i el nostre token. Ara cal obtenir el ```chat_id``` per poder enviar missatges a un grup de Telegram en concret.

![chat_id.png](Images/chat_id.png)



Per enviar un Telegram el millor és crear un grup i esbrinar el ```chat_id``` d’aquell grup. 

Crearem un grup i afegim el bot que acabem de crear. (En l’exemple el bot: IOCTallersBot Jornades Tècniques)

Ara enviem qualsevol text des del grup IOCTallersBot. Per exemple el missatge: hola. Prova.

![chat_id2.png](Images/chat_id2.png)

I obrim en un navegador web aquesta adreça, substituint el token del nostre bot, on ho indica a la URL:

```https://api.telegram.org/bot<YourBOTToken>/getUpdates```

_( sense els signes ‘<’ ‘ > ’)_

Busquem el nostre grup i obtindrem el chat_id   


![chat_id3.png](Images/chat_id3.png)









