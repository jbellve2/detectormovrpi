#coding=utf-8
#
# 05-2018
# Josep Vicent Bellver Castelló
# ('')--,--,-

import time
import sys
import os

arxiu='cam.jpg'                 # nom de la foto

#if len(sys.argv) < 2:
#       arxiu='cam.jpg'
#else:
path=os.getenv("HOME")
arxiu=path+'/'+arxiu            # si executa l'usuari root, el path ées : /root
print('arxiu='+arxiu)           # mostra per pantalla la ruta+foto


#install python3-picamera       # Recorda també d'habilitar la càmera des de la Raspberry

from picamera import PiCamera
try:
        camera = PiCamera()
#       camera.resolution = (2592, 1944)
        camera.resolution = (640,480)
        camera.rotation=-180
        camera.start_preview()
#       # preparant la càmera per a la primera foto
        time.sleep(2)
###     camera.capture('cam.png', format='png', use_video_port=True)
##      camera.capture('cam.jpg', format='jpg', use_video_port=True)
        camera.capture(arxiu, format='png', resize=(640,480), use_video_port=True)


except Exception as e:
        print(e)

finally:
        camera.close()
