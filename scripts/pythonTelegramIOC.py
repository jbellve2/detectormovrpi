# coding=utf-8
#
# Script en python que envia un telegram s'utilitza axi:
# python nomPrograma.py "text"
#
# Josep Vicent Bellver i Castello
# 12-12-2021
# ('')--,--,-

import time                                     # PER AGAFAR L'HORA DEL SISTEMA
from time import localtime,gmtime,strftime
from datetime import datetime
import os
from os import remove
import sys
import requests                                 # CAL INSTAL·LAR: requests.
                                                # AIXÍ:  pip install requests ... o pip3 install requests

###############################################################################################################
###############  SI CREUS QUE EL TOKEN HA SIGUT DESCOBERT, ES POT CANVIAR DESDE EL BOTFATHER ##################
#
## bot TELEGRAM IOC
#_TOKEN="5470026738:AAFMPDAf_qNOM7OTxL46IbyicBU9uh_H1EY"        # CODI DE SEGURETAT PER PODER ACCEDIR AL NOSTRE BOT
_TOKEN="5470026738:AAHNERYi60lyCHuFpm_Edx1PkwC6tWLiBbI"        # CODI DE SEGURETAT PER PODER ACCEDIR AL NOSTRE BOT
id="-867511046"                                                # CODI DEL GRUP DE TELEGRAM ON S'ENVIARAN ELS MISSATGES


###############################################################################################################

############
# FUNCIONS #
#
# Funció per enviar un Telegram amb una imatge
def send_photo(chat_id, image_path, image_caption=""):
    data = {"chat_id": chat_id, "caption": image_caption}
    url='https://api.telegram.org/bot'+_TOKEN+'/sendPhoto?chat_id='+chat_id

    with open(image_path, "rb") as image_file:
        ret = requests.post(url, data=data, files={"photo": image_file})
#        #return ret.json()


#
#
# Funció per enviar un missatge de text Telegram
def send_text(chat_id, text):

       send_text = 'https://api.telegram.org/bot' + _TOKEN + '/sendMessage?chat_id=' + chat_id + '&parse_mode=Markdown&text=' + text
       response = requests.get(send_text)


#############
# PRINCIPAL #

text=sys.argv[1]                # RECOLLIM EL TEXT QUE ENS PASSEN PER PARÀMETRE AL EXECUTAR L'SCRIPT
send_text(id,text)              # EXECUTEM LA FUNCIÓ CREADA ABANS PER L'ENVIAMENT DE TEXT AMB TELEGRAM

#dataAvui=str(datetime.today().strftime('%Y-%m-%d_%H.%M.%S'))   # AGAFEM LA DATA I HORA ACTUAL


### COMENTA AQUEST CODI SI NO TENS CÀMERA
### PER ENVIAR UNA IMATGE:
#
#
img="cam.jpg"                   # nom de la foto
path=os.getenv("HOME")          # carpeta de la foto
img=path+'/'+img             	# carpeta + foto. Si executa l'usuari root, el path és : /root
dataAvui=datetime.now()         # agafem la data i horad'avui
send_photo(id,img,dataAvui)	# Utilita la funció d'enviar una imatge
print("foto telegram enviada")  # Informem per pantalla
#
#
#remove(img)                     # Eliminem la imatge del disc dur

