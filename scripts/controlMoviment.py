# coding=utf-8
# Script en python per capturar la senyal que emet un mòdul de moviment PIR.
# En el moment que detecta moviment, entrarà una senyal i enviarà un Telegram, utilitzant un l'script pythonTelegramIOC.py
#
# Ús: python nomPrograma.py
#
# Josep Vicent Bellver i Castello
# 12-12-2021
# ('')--,--,-

import RPi.GPIO as GPIO                                 # llibreries de python que controlen els GPIO
import os
import sys
import time                                             # llibreria de temps
from datetime import datetime                           # per esbrinar el dia/hora d'avui
pinGPIO=11                                              # Numero de PIN on hem connectat el Modul PIR
pinLED=3                                                # Si connectem un LED al PIN 3 (per exemple)
GPIO.setwarnings(False)                                 # ignorem els warnings
GPIO.setmode(GPIO.BOARD)                                # utilitzem la numeracio BOARD enlloc de la GPIO
GPIO.setup(pinGPIO,GPIO.IN)                             # gpio d'entrada des de la PIR Motion. PIN 11
GPIO.setup(pinLED,GPIO.OUT)                             # gpio de sportida cap a un LED connectat en el PIN 3

os.system("clear")                                      # Borrem la pantalla
while True:                                             # Bucle infinit
        i=GPIO.input(pinGPIO)                           # PIN connectat a PIR. Ens retorna 1 si s'activa o 0 si no s'activa
        if i==0:                                        # quant la sortida del sensor de moviment es LOW
                ## CAP INTRUS

                GPIO.output(pinLED,0)                 	# apaguem el led (en el cas de tenir-lo. I si es aixi, al pin num3)
                time.sleep(1)                           # parem un segon
                print("cap intrus")
        else:
		## INTRÚS DETECTAT

                GPIO.output(pinLED,1)                  	# engeguem el led (en el cas de tenir-lo. I si es aixi, al pin num3)
                time.sleep(0.1)
                print("Intrus detectat!!")              # avisem per consola


		### FES FOTO ###
                os.system("python /home/pi/scripts/fesFoto.py")        # fem la foto
		arxiu="cam.jpg"				# nom de la foto
		path=os.getenv("HOME")			# carpeta de la foto
		arxiu=path+'/'+arxiu            	# carpeta + foto. Si executa l'usuari root, el path és : /root


                ### TELEGRAM AVISANT ###
                avui=datetime.now()                     # agafem la data i horad'avui
                avuiSTR=avui.strftime("%d/%m/%Y, %H:%M:%S")             # la posem en una variable STRING
                missatge=("Hi ha hagut un intrús avui: "+avuiSTR)       # creem el missatge
                os.system("python /home/pi/scripts/pythonTelegramIOC.py '"+missatge+"'")        # enviem el missatge
                print("esperant 40 segons\n")           # informem a l'usuari de l'espera
                time.sleep(40)                          # esperem un temps (40 segons). Per no saturar l'enviament de correu
